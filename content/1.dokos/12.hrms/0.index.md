---
title: Bienvenue
description: Application de gestion RH pour Dokos
icon: icon-park-outline:fireworks
---

# Introduction

:ellipsis

HRMS est une application ajoutant des fonctionnalités de gestion RH à Dokos.  
Vous pouvez l'installer en suivant les instructions disponibles sur le [répertoire de l'application](https://gitlab.com/dokos/hrms)  
Cette application est installée par défaut sur tous les sites du Cloud Dokos et dans les images Docker de Dokos.

::alert{type="info"}
L'application HRMS est une adaptation de l'application Frappe HR par <a href="https://github.com/frappe/hrms" target="_blank">Frappe Technologies</a>
::

::card-grid
#title
Les principales fonctionnalités du module HRMS

#default
  ::link-card
  ---
  href: /dokos/hrms/cycle-de-vie
  icon: clarity:employee-solid
  ---
  #title
  Cycle de vie des employés
  #description
  Créez vos employés et votre structure organisationnelle et suivez le cycle de vie de vos collaborateurs
  ::

  ::link-card
  ---
  href: /dokos/hrms/conges
  icon: fontisto:holiday-village
  ---
  #title
  Congés
  #description
  Gérez les allocations et demandes de congés de vos employés
  ::

  ::link-card
  ---
  href: /dokos/hrms/quarts-et-presences
  icon: fluent:shifts-activity-24-filled
  ---
  #title
  Gestion des quarts et présences
  #description
  Gérez les présences et les allocations de quart de vos employés
  ::

  ::link-card
  ---
  href: /dokos/hrms/paie
  icon: ic:outline-payments
  ---
  #title
  Gestion de la paie
  #description
  Gérez la paie de votre organisation  
  *Ce module n'est pas encore adapté à la France*
  ::
::