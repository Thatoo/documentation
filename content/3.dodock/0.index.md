---
title: Bienvenue
description: Framework applicatif low-code et open-source
icon: icon-park-outline:fireworks
---

# Bienvenue

Dodock est un modèle applicatif low-code permettant de créer des applications métier rapidement. Il sert de moteur aux application de l'éco-système Dokos.  

::alert{type="info"}
L'application Dokos est une adaptation de l'application ERPnext par <a href="https://github.com/frappe/frappe" target="_blank">Frappe Technologies</a>
::


::card-grid
#title
Les principales fonctionnalités de Dodock

#root
:ellipsis

#default
  ::card
  ---
  icon: carbon:container-software
  ---
  #title
  Interface administrateur
  #description
  Définissez vos formulaires et bénéficiez d'un interface utilisateur avec des espaces de travail, des listes, des calendriers, des rapports et plus encore.
  ::

  ::card
  ---
  icon: eos-icons:role-binding-outlined
  ---
  #title
  Rôles et autorisations
  #description
  Donnez des accès restreints à certaines fonctionnalités à vos différentes catégories d'utilisateurs.
  ::

  ::card
  ---
  icon: octicon:workflow-24
  ---
  #title
  Workflows
  #description
  Mettez en place des flux de validation métier en quelques clics pour n'importe quel type de document.
  ::

  ::card
  ---
  icon: mdi:api
  ---
  #title
  API REST
  #description
  Tous vos formulaires sont associés à une API REST et peuvent déclencher des Webhooks.
  ::

  ::card
  ---
  icon: codicon:terminal-powershell
  ---
  #title
  Low Code
  #description
  Proposez des fonctionnalités standards à vos utilisateurs et laissez les étendre grâce aux outils low-code intégrés.
  ::

  ::card
  ---
  icon: mdi:toolbox-outline
  ---
  #title
  Boîte à outils
  #description
  Oauth/OpenID, gestionnaire de tâches de fonds, Socket.io, Redis Pub/Sub,... Dodock est fourni avec tous les outils pour créer des applications métier puissantes, stables et évolutives.
  ::
::